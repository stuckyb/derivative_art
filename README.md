This is a version of the ART sequencing read simulator (see [here](https://doi.org/10.1093/bioinformatics/btr708) and [here](https://www.niehs.nih.gov/research/resources/software/biostatistics/art/index.cfm)) that has been modified to include simulation of adapter read-through on Illumina sequencing instruments.  It is based on ART version "ART-MountRainier-2016-06-05".  It also includes new error profiles for 300-bp reads on Illumina MiSeq sequencers, which make it possible to simulate 300-bp reads.


## Build instructions

I strongly suggest doing an out-of-source build so you don't clutter up the source directory with build artifacts.  To do this, follow these steps.

1. Make the source directory your working directory.
2. `$ mkdir build`
3. `$ cd build`
4. `$ ../configure`
5. `$ make`

After running `configure` once, you don't need to run it again for subsequent build operations.  Simply run `make` to update the compiled binaries as needed.


## Key changes/additions in comparison to the original ART

1. For paired-end read simulation, library fragments smaller than the read size are allowed, and if the fragment is smaller than the read size, adapter read-through is simulated.
1. If, after reading through the adapter, the read sequence is still less than the read size, the remainder of the read length is filled with random, 'A'-biased nucleotides.  This "filling" strategy is the same as that used by the simulator [PERsim](https://github.com/imgag/ngs-bits).
1. Two new command-line options have been added, `--adapter_fwd` and `--adapter_rev`, that allow for the specification of custom forward and reverse read adapter sequences.
1. The sequence titles for the reads in the output FASTQ files include the actual insert size for each read.  The reported insert size accounts for any indel sequencing errors (e.g., if there was a deletion in the read data for a library fragment, the insert size will be 1 less than the fragment size mapped onto the reference sequence.)
1. The `r_prob()` function, which is used for random number generation across ART, now uses the new, high-quality random number generating library introduced in C++ 11.
1. Original ART uses the [GSL](https://www.gnu.org/software/gsl/) function `gsl_ran_gaussian()` to simulate draws from a normal distribution.  In testing on my machine, this was generating the exact same sequence of library fragment lengths every time I ran ART.  To fix this, I replaced the use of `gsl_ran_gaussian()` for generating paired-end reads with `std::normal_distribution` from the `random` library introduced in C++ 11.
1. Original ART does not include any error profiles for 300-bp reads on Illumina instruments, which means it is not possible to simulate read lengths greater than 250 bp.  This version of ART includes built-in profiles for simulating 300-bp reads from MiSeq sequencers.


## Limitations

1. Adapter read-through is _only_ simulated for ordinary paired-end read simulation (i.e., simulation using the `-p`/`--paired` option), and not for any other simulation modes.
2. The new method for generating samples from a normal distribution, described above, _only_ applies to paired-end read simulation.  Any other uses of a normal distribution in ART still rely on the old `gsl_ran_gaussian()` method.
3. Random number generators for paired-end read simulation are always seeded from the system clock.  That is, for paired-end read simulation, the command-line option `-rs`/`--rndSeed` has no effect.
4. All of the modifications described here apply _only_ to the `art_illumina` utility.  All other sequence simulation utilities in ART are unchanged.


## Default sequencing instruments and error profiles

When `art_illumina` is run with a target read length and no sequencing instrument is specified, a suitable error profile for the given read length is automatically selected.  Which error profiles are used as the defaults for different read lengths does not seem to be clearly documented anywhere, so I am adding that documentation here for some commonly used read lengths.

* 100 bp: HiSeq 2000
* 150 bp: HiSeq 2500
* 250 bp: MiSeq v1
* 300 bp: MiSeq (new in this modified version of ART)

